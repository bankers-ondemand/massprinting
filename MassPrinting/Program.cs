﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using BWG.Logger;

using RestSharp;
using System.Net.Http;

using System.Threading.Tasks;
using System.Net.Mail;

namespace MassPrinting
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DataNormalizationEntities.PRD"].ConnectionString;
            var apiTimeOut = ConfigurationManager.AppSettings["apiTimeout"];
            string selectStatement = @"SELECT *
                                       FROM [DataNormalization].[dbo].[MassPrinting_Automation_Config]
                                       WHERE Enable = 1 
                                       ORDER BY OrderBy ASC";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand comm = new SqlCommand(selectStatement, connection))
                {
                    try
                    {
                        connection.Open();
                        using (SqlDataReader dr = comm.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                var FileName = string.Empty;
                                while (dr.Read())
                                {
                                    try
                                    {
                                        string sConfig = string.Empty;
                                        if (dr["Process"].ToString() == "File_Transfer")
                                            sConfig = System.Configuration.ConfigurationManager.AppSettings["DNAPIFT"];
                                        if (dr["Process"].ToString() == "ZIP_Configuration")
                                            sConfig = System.Configuration.ConfigurationManager.AppSettings["DNAPIZP"];

                                        string url = string.Format("{0}{1}", sConfig, dr["Id"].ToString());
                                        Logger_NoDealerId.LogMessage("url = " + url, null, aLogger.LogLevel.INFO);

                                        var response = Execute(url,apiTimeOut);
                                        if (response.IsSuccessful)
                                            Logger_NoDealerId.LogMessage("The API call result was " + response.StatusCode, null, aLogger.LogLevel.INFO);
                                        else
                                        {
                                            Logger_NoDealerId.LogMessage($"The API call result was {response.StatusCode} -  {response.ErrorMessage}", null, aLogger.LogLevel.INFO);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger_NoDealerId.LogMessage("Error calling API: " + ex.Message, null, BWG.Logger.aLogger.LogLevel.ERROR);
                                    }
                                }
                            }
                            else Console.WriteLine("No data to display");
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error: " + e.Message);
                        Logger_NoDealerId.LogMessage("Error: " + e.Message, null, BWG.Logger.aLogger.LogLevel.ERROR);
                    }
                    if (connection.State == System.Data.ConnectionState.Open)
                        connection.Close();
                }
            }
        }
        public static IRestResponse Execute(string url, string apiTimeOut)
        {
            var stringSplit = "api/";

            var urlArray = url.Split(new[] { stringSplit }, StringSplitOptions.None);
            var apiBaseUrl = $"{urlArray[0]}{stringSplit}";
            var client = new RestClient(apiBaseUrl);
            int minutes = Int32.Parse(apiTimeOut);
            client.Timeout = minutes * 60 * 1000;

            var request = new RestRequest(Method.POST);
            request.Resource = urlArray[1];
            //request.RootElement = "dealer";

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");


            var response = client.Execute(request);

            if (!response.IsSuccessful && response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                request.Parameters.Clear();
                response = client.Execute(request);
            }

            return response;
        }
    }
}
